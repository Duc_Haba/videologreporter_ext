# VideoLogReporter

This library helps you to track user interactions within the app by recording video and providing upload support.

Please add/change the token in the App Delegate 'didFinishLaunch' method before running the app.

### Please note that this is not AppStore safe and has to be used only in case of Debug mode or Client releases only.

## Requirements

* XCode 9.4.1 and later
* iOS 9.0 and later
* Swift/Objective-C project support


## Installation

Make sure you have cocoapods set up in your system and you already have initialized a podfile.
If not, setup your pod by using this link to [CocoaPods](https://www.appcoda.com/cocoapods/)

* Open your Pod file
- Add the source url of the specs repo at the top before listing the pods to install

```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://bitbucket.org/ymedialabs/yml-podspec.git'
```

The first source is the URL of the public pods available by Cocoapods.
The second source is the YML specs repo which has details of the private pods.

Add the following pod

```
pod 'VideoLogReporter'
```

* Navigate to project directory through terminal where your pod file is located and run the following command:

```
pod install
```

## Usage

You can initialize the recorder on App launch by using any of the following commands -


### Swift
```SRScreenRecorder.initializeRecorder(with: window, andAccessToken: <Dropbox Access Token Here>)```
```SRScreenRecorder.initializeRecorder(with: window, apiToken: <JIRA API Token>, projectKey: <Project Key>, parentIssue: <Parent Issue>, issueReporterMailID: <Reporter Mail ID>, assigneeUsername: <Assignee User Name>)```


### Objective-C
```[SRScreenRecorder initializeRecorderWithWindow:self.window andAccessToken:<Dropbox Access Token Here>];```
```[SRScreenRecorder initializeRecorderWithWindow:self.window apiToken:<JIRA API Token> projectKey:<Project Key> parentIssue:<Parent Issue> issueReporterMailID:<Reporter Mail ID> assigneeUsername:<Assignee User Name>]```


Window is the keyWindow for the app.

### Dropbox Token
You can generate the Dropbox access token by visiting the [Dropbox link](https://www.dropbox.com/developers/apps/create). You'll land on the attached page for creating the App. Select the options as shown in the image. After the app is created, selected Generated Access Token to generate the required token.

### Please note that if the dropbox token / JIRA token is regenerated, then you need to manually update the token inside the app as well.

### ![Screenshot](/Screenshots/CreateApp.png)
### ![Screenshot](/Screenshots/AppCreated.png)


### Dropbox Token
You can generate the JIRA API token by visiting the [JIRA link](https://id.atlassian.com/). Select API tokens and Click `Create API token`. Then copy the generated token.


## Author

Varun PM, varun.pm@ymedialabs.com


## License

VideoLogReporter is available under the MIT license. See the LICENSE file for more info.
