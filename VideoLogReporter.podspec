#
# Be sure to run `pod lib lint VideoLogReporter.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'VideoLogReporter'
s.version          = '1.3.2'
s.summary          = 'Provides app screen recording and uploading support for debugging.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = <<-DESC
This library helps you to track user interactions withing the app by recording video and providing upload support.
DESC

s.homepage         = 'https://bitbucket.org/ymedialabs/videologreporter'
s.license          = 'LICENSE'
s.author           = { 'Varun PM' => 'varun.pm@ymedialabs.com' }
s.source           = { :git => 'https://bitbucket.org/ymedialabs/videologreporter.git', :tag => s.version.to_s }

# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.platform     = :ios, "9.0"
s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4.0' }
`echo "4.0" > .swift-version`

s.requires_arc = true
s.source_files = 'VideoLogReporter/Classes/**/*', 'VideoLogReporter/Headers/**/*'
s.private_header_files = 'VideoLogReporter/Headers/Private/*.h'


# s.resource_bundles = {
#   'VideoLogReporter' => ['VideoLogReporter/Assets/*.png']
# }

# s.public_header_files = 'Pod/Classes/**/*.h'
# s.frameworks = 'UIKit', 'MapKit'

s.dependency 'ObjectiveDropboxOfficial', '~> 3.9'
s.dependency 'box-ios-sdk', '~> 1.0'
end
