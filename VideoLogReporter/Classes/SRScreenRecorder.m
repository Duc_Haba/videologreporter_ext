//
//  SRScreenRecorder.m
//  ScreenRecorder
//
//  Created by YMediaLabs on 2018/03/26.
//  Copyright © 2018 YMediaLabs. All rights reserved.
//

#import "SRScreenRecorder.h"
#import "KTouchPointerWindow.h"
#import <ObjectiveDropboxOfficial/ObjectiveDropboxOfficial.h>
#import "box-ios-sdk-umbrella.h"
#import "JIRAIssueViewController.h"

#define DEFAULT_FRAME_INTERVAL 2
#define DEFAULT_AUTOSAVE_DURATION 30
#define TIME_SCALE 600

// Constant to maitain video files until a crash occurs if any so as to maintain the steps that caused the crash
#define DEFAULT_CRASH_AUTOSAVE_DURATION 1

@interface SRScreenRecorder ()

/**
 A window to be recorded.
 */
@property (retain, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSString *accessToken;

// AVAsset helper properties for recording
@property (strong, nonatomic) AVAssetWriter *writer;
@property (strong, nonatomic) AVAssetWriterInput *writerInput;
@property (strong, nonatomic) AVAssetWriterInputPixelBufferAdaptor *writerInputPixelBufferAdaptor;
@property (assign, nonatomic) BOOL isCurrentlyWriting;

// Used for tracking frame interval
@property (strong, nonatomic) CADisplayLink *displayLink;

@property (assign, nonatomic) CFAbsoluteTime firstFrameTime;

// Dropbox upload client
@property (strong, nonatomic) DBUserClient *client;

// Box upload keys
@property (strong, nonatomic) NSString *boxClientID;
@property (strong, nonatomic) NSString *boxClientSecret;

// JIRA upload keys
@property (strong, nonatomic) NSString *apiToken;
@property (strong, nonatomic) NSString *projectKey;
@property (strong, nonatomic) NSString *parentIssue;
@property (strong, nonatomic) NSString *assigneeUsername;
@property (strong, nonatomic) NSString *reporterMailID;

@end

@implementation SRScreenRecorder {
    CFTimeInterval startTimestamp;
    BOOL shouldRestart;
    
    dispatch_queue_t queue;
    UIBackgroundTaskIdentifier backgroundTask;
}

static SRScreenRecorder *screenRecorder = nil;

+ (void)initializeRecorderWithWindow:(UIWindow *)window andAccessToken:(NSString *)accessToken
{
    if (screenRecorder == nil) {
        screenRecorder = [[SRScreenRecorder alloc] initWithWindow: window];
    }
    
    screenRecorder.accessToken = accessToken;
}

+ (void)initializeRecorderWithWindow:(UIWindow *)window clientId:(NSString *)clientID clientSecret:(NSString *)clientSecret
{
    if (screenRecorder == nil) {
        screenRecorder = [[SRScreenRecorder alloc] initWithWindow: window];
    }
    
    screenRecorder.boxClientID = clientID;
    screenRecorder.boxClientSecret = clientSecret;
}

+ (void)initializeRecorderWithWindow:(UIWindow *)window apiToken:(NSString *)apiToken projectKey:(NSString *)projectKey parentIssue:(NSString *)parentIssue issueReporterMailID:(NSString *)reporterMailID assigneeUsername:(NSString *)assigneeUsername
{
    if (screenRecorder == nil) {
        screenRecorder = [[SRScreenRecorder alloc] initWithWindow: window];
    }
    
    screenRecorder.apiToken = apiToken;
    screenRecorder.projectKey = projectKey;
    screenRecorder.parentIssue = parentIssue;
    screenRecorder.reporterMailID = reporterMailID;
    screenRecorder.assigneeUsername = assigneeUsername;
}

- (void)setupScreenRecorder
{
    // Stitch the crashed video, if present.
    [screenRecorder stitchVideoIfNeeded:YES];
}

+ (SRScreenRecorder *)sharedScreenRecorder
{
    return screenRecorder;
}

- (instancetype)initWithWindow:(UIWindow *)window
{
    self = [super init];
    if (self) {
        _window = window;
        _frameInterval = DEFAULT_FRAME_INTERVAL;
        _autosaveDuration = DEFAULT_AUTOSAVE_DURATION;
        _showsTouchPointer = YES;
        
        NSString *label = [NSString stringWithFormat:@"com.logger.screen_recorder"];
        queue = dispatch_queue_create([label cStringUsingEncoding:NSUTF8StringEncoding], NULL);
        
        [self setupNotifications];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self stopRecording];
}

#pragma mark Setup

- (void)setupAssetWriterWithURL:(NSURL *)outputURL
{
    NSError *error = nil;
    
    self.writer = [[AVAssetWriter alloc] initWithURL:outputURL fileType:AVFileTypeQuickTimeMovie error:&error];
    NSParameterAssert(self.writer);
    if (error) {
        NSLog(@"Error: %@", [error localizedDescription]);
    }
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGSize size = mainScreen.bounds.size;
    
    NSDictionary *outputSettings = @{AVVideoCodecKey : AVVideoCodecH264, AVVideoWidthKey : @(size.width), AVVideoHeightKey : @(size.height)};
    self.writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:outputSettings];
    self.writerInput.expectsMediaDataInRealTime = YES;
    
    NSDictionary *sourcePixelBufferAttributes = @{(NSString *)kCVPixelBufferPixelFormatTypeKey : @(kCVPixelFormatType_32ARGB)};
    self.writerInputPixelBufferAdaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:self.writerInput
                                                                                                          sourcePixelBufferAttributes:sourcePixelBufferAttributes];
    NSParameterAssert(self.writerInput);
    NSParameterAssert([self.writer canAddInput:self.writerInput]);
    
    [self.writer addInput:self.writerInput];
    
    self.firstFrameTime = CFAbsoluteTimeGetCurrent();
    
    [self.writer startWriting];
    [self.writer startSessionAtSourceTime:kCMTimeZero];
}

- (void)setupTouchPointer
{
    if ((self.showsTouchPointer && self.isRecording) || shouldRestart) {
        KTouchPointerWindowInstall();
    } else {
        KTouchPointerWindowUninstall();
    }
}

- (void)setupNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)setupTimer
{
    self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(captureFrame:)];
    self.displayLink.frameInterval = self.frameInterval;
    [self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

#pragma mark Recording

- (void)startRecording
{
    self.isRecording = YES;
    [self setupAssetWriterWithURL:[self outputFileURL]];
    
    [self setupTouchPointer];
    
    [self setupTimer];
}

- (void)stopRecording
{
    [self stopRecordingWithSaveCallBack:nil];
}

- (void)stopRecordingWithSaveCallBack:(FileSaveCompleted)saveCallBack
{
    self.isRecording = NO;
    [self.displayLink invalidate];
    startTimestamp = 0.0;
    [self setupTouchPointer];
    
    dispatch_async(queue, ^{
        if (self.writer.status != AVAssetWriterStatusCompleted && self.writer.status != AVAssetWriterStatusUnknown) {
            [self.writerInput markAsFinished];
        }
        
        if (self.writer.status == AVAssetWriterStatusWriting && !self.isCurrentlyWriting) {
            self.isCurrentlyWriting = YES;
            typeof(self) __weak weakSelf = self;
            [weakSelf.writer finishWritingWithCompletionHandler:^
             {
                 weakSelf.isCurrentlyWriting = NO;
                 [weakSelf finishBackgroundTask];
                 [weakSelf restartRecordingIfNeeded];
                 
                 if (saveCallBack) {
                     saveCallBack();
                 }
             }];
        }
    });
}

- (void)restartRecordingIfNeeded
{
    if (shouldRestart) {
        shouldRestart = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self startRecording];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            // User forcefully stopped recording. So not a crash
            [self stitchVideoIfNeeded:NO];
        });
    }
}

- (void)rotateFile
{
    shouldRestart = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopRecording];
    });
}

- (void)captureFrame:(CADisplayLink *)displayLink
{
    dispatch_async(queue, ^{
        if (self.writerInput.readyForMoreMediaData) {
            CVReturn status = kCVReturnSuccess;
            CVPixelBufferRef buffer = NULL;
            CFTypeRef backingData;
            
            __block UIImage *screenshot = nil;
            dispatch_sync(dispatch_get_main_queue(), ^{
                screenshot = [self screenshot];
            });
            CGImageRef image = screenshot.CGImage;
            
            CGDataProviderRef dataProvider = CGImageGetDataProvider(image);
            CFDataRef data = CGDataProviderCopyData(dataProvider);
            backingData = CFDataCreateMutableCopy(kCFAllocatorDefault, CFDataGetLength(data), data);
            CFRelease(data);
            
            const UInt8 *bytePtr = CFDataGetBytePtr(backingData);
            
            status = CVPixelBufferCreateWithBytes(kCFAllocatorDefault,
                                                  CGImageGetWidth(image),
                                                  CGImageGetHeight(image),
                                                  kCVPixelFormatType_32BGRA,
                                                  (void *)bytePtr,
                                                  CGImageGetBytesPerRow(image),
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  &buffer);
            NSParameterAssert(status == kCVReturnSuccess && buffer);
            
            if (buffer) {
                CFAbsoluteTime currentTime = CFAbsoluteTimeGetCurrent();
                CFTimeInterval elapsedTime = currentTime - self.firstFrameTime;
                
                CMTime presentTime =  CMTimeMake(elapsedTime * TIME_SCALE, TIME_SCALE);
                
                if(![self.writerInputPixelBufferAdaptor appendPixelBuffer:buffer withPresentationTime:presentTime]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self stopRecording];
                    });
                }
                
                CVPixelBufferRelease(buffer);
            }
            
            CFRelease(backingData);
        }
    });
    
    if (startTimestamp == 0.0) {
        startTimestamp = displayLink.timestamp;
    }
    
    NSTimeInterval delta = displayLink.timestamp - startTimestamp;
    
    if (DEFAULT_CRASH_AUTOSAVE_DURATION > 0 && delta > DEFAULT_CRASH_AUTOSAVE_DURATION) {
        startTimestamp = 0.0;
        [self rotateFile];
    }
}

- (UIImage *)screenshot
{
    UIScreen *mainScreen = [UIScreen mainScreen];
    CGSize imageSize = mainScreen.bounds.size;
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *window in windows) {
        if (![window respondsToSelector:@selector(screen)] || window.screen == mainScreen) {
            CGContextSaveGState(context);
            
            CGContextTranslateCTM(context, window.center.x, window.center.y);
            CGContextConcatCTM(context, [window transform]);
            CGContextTranslateCTM(context,
                                  -window.bounds.size.width * window.layer.anchorPoint.x,
                                  -window.bounds.size.height * window.layer.anchorPoint.y);
            
            [window.layer.presentationLayer renderInContext:context];
            
            CGContextRestoreGState(context);
        }
    }
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

#pragma mark Background tasks
- (void)applicationDidEnterBackground:(NSNotification *)notification
{
    UIApplication *application = [UIApplication sharedApplication];
    
    UIDevice *device = [UIDevice currentDevice];
    BOOL backgroundSupported = NO;
    if ([device respondsToSelector:@selector(isMultitaskingSupported)]) {
        backgroundSupported = device.multitaskingSupported;
    }
    
    if (backgroundSupported) {
        backgroundTask = [application beginBackgroundTaskWithExpirationHandler:^{
            [self finishBackgroundTask];
        }];
    }
    
    [self stopRecording];
}

- (void)applicationWillEnterForeground:(NSNotification *)notification
{
    [self finishBackgroundTask];
}

- (void)finishBackgroundTask
{
    if (backgroundTask != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
        backgroundTask = UIBackgroundTaskInvalid;
    }
}

#pragma mark Utility methods

- (NSString *)documentDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [[paths objectAtIndex:0] stringByAppendingString:@"/ScreenRecorderVideos"];
    return documentsDirectory;
}

- (NSString *)defaultFilename
{
    time_t timer;
    time(&timer);
    NSString *timestamp = [NSString stringWithFormat:@"%ld", timer];
    return [NSString stringWithFormat:@"%@.mp4", timestamp];
}

- (BOOL)existsFile:(NSString *)filename
{
    if ([[[NSFileManager alloc] init] createDirectoryAtPath:[self documentDirectory] withIntermediateDirectories:YES attributes:nil error:nil]) {
        NSString *path = [self.documentDirectory stringByAppendingPathComponent:filename];
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        BOOL isDirectory;
        return [fileManager fileExistsAtPath:path isDirectory:&isDirectory] && !isDirectory;
    }
    
    return NO;
}

- (NSString *)nextFilename:(NSString *)filename
{
    static NSInteger fileCounter;
    
    fileCounter++;
    NSString *pathExtension = [filename pathExtension];
    filename = [[[filename stringByDeletingPathExtension] stringByAppendingString:[NSString stringWithFormat:@"-%@", @(fileCounter)]] stringByAppendingPathExtension:pathExtension];
    
    if ([self existsFile:filename]) {
        return [self nextFilename:filename];
    }
    
    return filename;
}

- (NSURL *)outputFileURL
{
    NSString *filename = [self defaultFilename];
    if ([self existsFile:filename]) {
        filename = [self nextFilename:filename];
    }
    
    NSString *path = [self.documentDirectory stringByAppendingPathComponent:filename];
    return [NSURL fileURLWithPath:path];
}

#pragma mark Video stitching method if app crashed
- (void)stitchVideoIfNeeded:(BOOL)hasCrashed
{
    // Start for merging video files saved in documents directory
    NSURL *documentsDirectory = [NSURL fileURLWithPath:[self documentDirectory]];
    
    NSError *error = nil;
    NSArray<NSString *> *fileURLs = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:[documentsDirectory path] error:&error] sortedArrayUsingSelector:@selector(compare:)];
    
    // Merge video helper classes
    NSMutableDictionary<NSString *, AVMutableComposition *> *compositions = [[NSMutableDictionary alloc] init];
    __block AVMutableComposition *composition = nil;
    __block AVMutableCompositionTrack *videoTrack = nil;
    __block AVMutableCompositionTrack *audioTrack = nil;
    
    __block CMTime insertTime = kCMTimeZero;
    __block NSInteger mergedFileCount = 0;
    
    // Get the file URLs for each recorder video from documents directory
    [fileURLs enumerateObjectsUsingBlock:^(NSString *url, NSUInteger idx, BOOL *stop) {
        if ([url containsString:@".mp4"] && ![url hasPrefix:[self mergeFilePrefix]] && ![url hasPrefix:[self crashFilePrefix]]) {
            NSURL *filePath = [documentsDirectory URLByAppendingPathComponent:url];
            
            AVURLAsset *asset = [AVURLAsset assetWithURL:filePath];
            NSArray<AVAssetTrack *> *videoTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
            NSArray<AVAssetTrack *> *audioTracks = [asset tracksWithMediaType:AVMediaTypeAudio];
            
            if (mergedFileCount >= [self autosaveDuration]) {
                insertTime = kCMTimeZero;
                mergedFileCount = 0;
                
                // Set this value to nil so as to reset all variables to restart video file
                composition = nil;
            }
            
            if (!composition) {
                composition = [[AVMutableComposition alloc] init];
                videoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
                audioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
                
                // Store the composition
                [compositions setObject:composition forKey:url];
            }
            
            if ([videoTracks count]) {
                mergedFileCount += 1;
                
                AVAssetTrack *videoAssetTrack = videoTracks.firstObject;
                [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration) ofTrack:videoAssetTrack atTime:insertTime error:nil];
                AVAssetTrack *audioAssetTrack = audioTracks.firstObject;
                [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration) ofTrack:audioAssetTrack atTime:insertTime error:nil];
                insertTime = CMTimeAdd(insertTime, asset.duration);
            }
            
            NSError *error = nil;
            [[NSFileManager defaultManager] removeItemAtURL:filePath error:&error];
        }
    }];
    
    // If there is a merge video pending, then merge it and on success, upload it.
    if (compositions.count) {
        NSMutableArray<NSString *> *fileURLs = [[NSMutableArray alloc] init];
        
        dispatch_group_t group = dispatch_group_create();
        
        [compositions enumerateKeysAndObjectsUsingBlock:^(NSString *key, AVMutableComposition *composition, BOOL *stop) {
            // File name to identify if app crashed or normal recorded
            dispatch_group_enter(group);
            
            NSString *mergedFileName = [NSString stringWithFormat:@"%@%@", (hasCrashed ? [self crashFilePrefix] : [self mergeFilePrefix]), key];
            NSURL *completeRecordingURL = [documentsDirectory URLByAppendingPathComponent:mergedFileName];
            
            AVAssetExportSession *exporter = [AVAssetExportSession exportSessionWithAsset:composition presetName:AVAssetExportPresetPassthrough];
            exporter.outputURL = completeRecordingURL;
            exporter.outputFileType = AVFileTypeMPEG4;
            
            [fileURLs addObject:completeRecordingURL.lastPathComponent];
            
            [exporter exportAsynchronouslyWithCompletionHandler:^{
                switch (exporter.status) {
                    case AVAssetExportSessionStatusCompleted:
                        break;
                        
                    default:
                        break;
                }
                
                dispatch_group_leave(group);
            }];
        }];
        
        dispatch_group_notify(group, dispatch_get_main_queue(), ^{
            [self videoSaveCompleted:fileURLs hasCrashed:hasCrashed];
        });
    }
    else {
        // Upload pending videos, if any.
        // Start for uploading video files saved in documents directory
        NSURL *documentsDirectory = [NSURL fileURLWithPath:[self documentDirectory]];
        
        NSError *error = nil;
        NSArray<NSString *> *fileURLs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[documentsDirectory path] error:&error];
        
        [self videoSaveCompleted:fileURLs hasCrashed:hasCrashed];
    }
}

- (NSString *)mergeFilePrefix {
    return @"merged-";
}

- (NSString *)crashFilePrefix {
    return @"crashed-";
}

#pragma mark Upload helper method
- (void)videoSaveCompleted:(NSArray<NSString *> *)fileURLs hasCrashed:(BOOL)hasCrashed {
    __block NSURL *documentsDirectory = [NSURL fileURLWithPath:[self documentDirectory]];
    NSMutableDictionary<NSURL *, DBFILESCommitInfo *> *fileContents = [[NSMutableDictionary alloc] init];
    
    // Generate folder structure as ProjectName/Version/BuildNo.
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *folderStructure = [NSString stringWithFormat:@"/%@/%@/%@/", (NSString *)[infoDictionary objectForKey:@"CFBundleName"], (NSString *)[infoDictionary objectForKey:@"CFBundleShortVersionString"], (NSString *)[infoDictionary objectForKey:(NSString *)kCFBundleVersionKey]];
    NSMutableArray<NSURL *> *filePaths = [[NSMutableArray alloc] init];
    
    // Get the file URLs for each recorder video from documents directory
    [fileURLs enumerateObjectsUsingBlock:^(NSString *url, NSUInteger idx, BOOL *stop) {
        if ([url containsString:@".mp4"]) {
            documentsDirectory = [documentsDirectory URLByAppendingPathComponent:url];
            [fileContents setObject:[[DBFILESCommitInfo alloc] initWithPath:[NSString stringWithFormat:@"%@%@", folderStructure, url]] forKey:documentsDirectory];
            [filePaths addObject:documentsDirectory];
            documentsDirectory = [documentsDirectory URLByDeletingLastPathComponent];
        }
    }];
    
    if ([filePaths count] > 0 && self.apiToken.length > 0) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self openJIRAVCWithFilePaths:filePaths hasCrashed:hasCrashed];
        });
    }
    
    if ([fileContents count] > 0 && self.accessToken.length > 0) {
        // Upload files to dropbox folder
        NSLog(@"Uploading files from - %@", documentsDirectory);
        
        if (!self.client) {
            self.client = [[DBUserClient alloc] initWithAccessToken:self.accessToken];
        }
        
        // Upload files sequentially since uploading in parallel was causing dropbox random crash
        [self uploadVideoWithContents:fileContents withSaveCallBack:^{
            NSLog(@"File uploading completed");
        }];
    }
    
    if ([filePaths count] > 0 && self.boxClientID.length > 0 && self.boxClientSecret.length > 0) {
        [BOXContentClient setClientID:self.boxClientID clientSecret:self.boxClientSecret];
        
        if ([[BOXContentClient defaultClient] user]) {
            [self uploadVideoToBoxWithFilePaths:filePaths saveCallBack:^{
                NSLog(@"File uploading completed");
            }];
        } else {
            [[BOXContentClient defaultClient] authenticateWithCompletionBlock:^(BOXUser *user, NSError *error) {
                [self uploadVideoToBoxWithFilePaths:filePaths saveCallBack:^{
                    NSLog(@"File uploading completed");
                }];
            }];
        }
    }
}

#pragma mark - Dropbox upload
- (void)uploadVideoWithContents:(NSMutableDictionary<NSURL *, DBFILESCommitInfo *> *)fileContents withSaveCallBack:(FileSaveCompleted)saveCallBack
{
    if (fileContents.count == 0) {
        saveCallBack();
        return;
    }
    
    NSURL *key = [[fileContents allKeys] firstObject];
    DBFILESCommitInfo *obj = [[fileContents allValues] firstObject];
    
    NSLog(@"Uploading file present at %@", key);
    
    typeof(self) __weak weakSelf = self;
    [self.client.filesRoutes batchUploadFiles:@{key: obj} queue:nil progressBlock:^(int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        NSLog(@"%lld bytes written out of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    } responseBlock:^(NSDictionary<NSURL *,DBFILESUploadSessionFinishBatchResultEntry *> *fileUrlsToBatchResultEntries, DBASYNCPollError *finishBatchRouteError, DBRequestError *finishBatchRequestError, NSDictionary<NSURL *,DBRequestError *> *fileUrlsToRequestErrors) {
        // Once upload is successful, delete all old files from documents directory
        NSArray<NSURL *> *uploadDataKeys = fileUrlsToBatchResultEntries.allKeys;
        
        [uploadDataKeys enumerateObjectsUsingBlock:^(NSURL *url, NSUInteger idx, BOOL *stop) {
            NSLog(@"Uploaded file present in %@", key);
            
            NSError *error = nil;
            [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
        }];
        
        [fileContents removeObjectForKey:key];
        
        [weakSelf uploadVideoWithContents:fileContents withSaveCallBack:saveCallBack];
    }];
}

#pragma mark - Box upload
- (void)uploadVideoToBoxWithFilePaths:(NSArray<NSURL *> *)filePaths saveCallBack:(FileSaveCompleted)saveCallBack
{
    
}

#pragma mark - JIRA upload
- (void)openJIRAVCWithFilePaths:(NSArray<NSURL *> *)filePaths hasCrashed:(BOOL)hasCrashed
{
    JIRAIssueViewController *jiraVC = [[JIRAIssueViewController alloc] initWithNibName:NSStringFromClass(JIRAIssueViewController.class) bundle:[NSBundle bundleForClass:JIRAIssueViewController.class]];
    jiraVC.apiToken = self.apiToken;
    jiraVC.projectKey = self.projectKey;
    jiraVC.parentIssue = self.parentIssue;
    jiraVC.reporterMailID = self.reporterMailID;
    jiraVC.assigneeUsername = self.assigneeUsername;
    jiraVC.filePaths = filePaths;
    jiraVC.hasCrashed = hasCrashed;
    
    [[self topViewController:[[UIApplication.sharedApplication keyWindow] rootViewController]] presentViewController:jiraVC animated:YES completion:nil];
}

- (UIViewController *)topViewController:(UIViewController *)controller
{
    if ([controller isKindOfClass:UINavigationController.class])
    {
        return [self topViewController:[(UINavigationController *)controller visibleViewController]];
    }
    
    if ([controller isKindOfClass:UITabBarController.class])
    {
        return [self topViewController:[(UITabBarController *)controller selectedViewController]];
    }
    
    if ([controller presentedViewController])
    {
        return [self topViewController:[controller presentedViewController]];
    }
    
    return controller;
}

@end
