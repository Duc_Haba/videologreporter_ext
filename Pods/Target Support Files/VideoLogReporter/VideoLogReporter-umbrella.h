#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "SRScreenRecorder.h"
#import "JIRAIssueViewController.h"
#import "KTouchPointerWindow.h"
#import "UIDevice-Hardware.h"
#import "UIWindow+SRScreenRecorderWindow.h"

FOUNDATION_EXPORT double VideoLogReporterVersionNumber;
FOUNDATION_EXPORT const unsigned char VideoLogReporterVersionString[];

