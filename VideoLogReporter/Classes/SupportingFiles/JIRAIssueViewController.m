//
//  JIRAIssueViewController.m
//  ObjectiveDropboxOfficial
//
//  Created by Varun P M on 09/07/18.
//

#import "JIRAIssueViewController.h"
#import "UIDevice-Hardware.h"

#define JIRA_BASE_URL "https://ymedialabs.atlassian.net/rest/api/2"

@interface JIRAIssueViewController ()

@property (weak, nonatomic) IBOutlet UITextField *issueTitleText;
@property (weak, nonatomic) IBOutlet UITextView *issueDescriptionText;

@property (nonatomic) UIView *errorView;
@property (nonatomic) UIView *mainView;

@end

@implementation JIRAIssueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIDevice *device = [UIDevice currentDevice];
    NSString *versionString = [NSString stringWithFormat:@"\n\n\n\n\n------------------------------------\nVersion: %@ (%@)", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    
    if (self.hasCrashed) {
        self.issueTitleText.text = @"Possible App Crash";
    }
    
    self.issueDescriptionText.text = [NSString stringWithFormat:@"%@\n%@\n%@ %@\n", versionString, [device modelName], device.systemName, device.systemVersion];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)createIssueButtonTapped:(id)sender {
    if (self.issueTitleText.text.length == 0) {
        [self showErrorBannerView:@"Please enter issue title"];
        
        return;
    }
    
    if (self.issueDescriptionText.text.length == 0) {
        [self showErrorBannerView:@"Please enter issue description"];
        
        return;
    }
    
    [self createIssue:self.issueTitleText.text andDescription:self.issueDescriptionText.text];
}

- (void)showErrorBannerView:(NSString *)message {
    if (self.errorView != nil) {
        return;
    }
    
    CGFloat topPadding = 0;
    
    if (@available(iOS 11.0, *)) {
        topPadding = self.view.safeAreaInsets.top;
    }
    
    self.errorView = [[UIView alloc] initWithFrame:CGRectMake(0, -44 - topPadding, self.view.bounds.size.width, 44 + topPadding)];
    self.errorView.backgroundColor = [UIColor redColor];
    UILabel *errorLabel = [[UILabel alloc] initWithFrame:self.errorView.bounds];
    errorLabel.textColor = [UIColor whiteColor];
    errorLabel.textAlignment = NSTextAlignmentCenter;
    errorLabel.text = message;
    [self.errorView addSubview:errorLabel];
    
    [self.view addSubview:self.errorView];
    
    [UIView animateWithDuration:0.75 animations:^{
        self.errorView.frame = CGRectMake(self.errorView.frame.origin.x, 0, self.errorView.frame.size.width, self.errorView.frame.size.height);
    } completion:^(BOOL finished) {
        [UIView animateKeyframesWithDuration:0.75 delay:2 options:UIViewKeyframeAnimationOptionAllowUserInteraction animations:^{
            self.errorView.frame = CGRectMake(self.errorView.frame.origin.x, -self.errorView.frame.size.height, self.errorView.frame.size.width, self.errorView.frame.size.height);
        } completion:^(BOOL finished) {
            [self.errorView removeFromSuperview];
            self.errorView = nil;
        }];
    }];
}

- (void)showProgress {
    self.mainView = [[UIView alloc] initWithFrame:self.view.bounds];
    [self.mainView setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4]];
    [self.mainView setAlpha:0];
    
    UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:self.mainView.bounds];
    [indicatorView setColor:[UIColor whiteColor]];
    [indicatorView startAnimating];
    
    [self.mainView addSubview:indicatorView];
    [self.view addSubview:self.mainView];
    
    [UIView animateWithDuration:0.6 animations:^{
        [self.mainView setAlpha:1];
    }];
}

- (void)hideProgress {
    [UIView animateWithDuration:0.6 animations:^{
        [self.mainView setAlpha:0];
    } completion:^(BOOL finished) {
        [self.mainView removeFromSuperview];
        self.mainView = nil;
    }];
}

- (void)createIssue:(NSString *)issueTitle andDescription:(NSString *)issueDescription {
    [self showProgress];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%s/issue", JIRA_BASE_URL]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:90];
    NSDictionary *httpBody = @{
        @"fields": @{
            @"project": @{
                @"key": self.projectKey
            },
            @"summary": issueTitle,
            @"description": issueDescription,
            @"issuetype": @{
                @"name": @"Sub-Task"
            },
            @"parent": @{
                @"key": self.parentIssue
            },
            @"assignee": @{
                @"name": self.assigneeUsername
            }
        }
    };
    
    NSData *authData = [[NSString stringWithFormat:@"%@:%@", self.reporterMailID, self.apiToken] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [authData base64EncodedStringWithOptions:0];
    
    [request setValue:@"Application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:[NSString stringWithFormat:@"Basic %@", base64Encoded] forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:httpBody options:NSJSONWritingPrettyPrinted error:nil]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data != nil) {
            NSError *jsonError = nil;
            NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
            
            NSDictionary *errors = [jsonData objectForKey:@"errors"];
            if (errors != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self hideProgress];
                    [self showErrorBannerView:(NSString *)[[errors allValues] firstObject]];
                });
            } else {
                [self uploadAttachment:(NSString *)[jsonData objectForKey:@"key"] fileURLs:self.filePaths];
            }
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hideProgress];
                [self showErrorBannerView:error.localizedDescription];
            });
        }
    }];
    
    [dataTask resume];
}

- (void)uploadAttachment:(NSString *)issueID fileURLs:(NSArray<NSURL *>*)fileURLs {
    if (fileURLs.count == 0) {
        [self hideProgress];
        [self dismissViewControllerAnimated:YES completion:nil];
        
        return;
    }
    
    NSString *boundary = [NSString stringWithFormat:@"Boundary-%@", [NSUUID UUID].UUIDString];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%s/issue/%@/attachments", JIRA_BASE_URL, issueID]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:90];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"nocheck" forHTTPHeaderField:@"X-Atlassian-Token"];
    [request setHTTPMethod:@"POST"];
    [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary] forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *postbody = [NSMutableData data];
    [postbody appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"video.mp4\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", @"video/mp4"] dataUsingEncoding:NSUTF8StringEncoding]];
    NSData *data = [NSData dataWithContentsOfURL:fileURLs.firstObject];
    
    [postbody appendData:data];
    [postbody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postbody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:postbody];
    
    NSData *authData = [[NSString stringWithFormat:@"%@:%@", self.reporterMailID, self.apiToken] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [authData base64EncodedStringWithOptions:0];
    
    [request setValue:[NSString stringWithFormat:@"Basic %@", base64Encoded] forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data != nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSFileManager defaultManager] removeItemAtURL:fileURLs.firstObject error:nil];
                
                NSMutableArray *tempFileURLs = (NSMutableArray *)[fileURLs mutableCopy];
                [tempFileURLs removeObjectAtIndex:0];
                [self uploadAttachment:issueID fileURLs:tempFileURLs];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hideProgress];
                [self showErrorBannerView:error.localizedDescription];
            });
        }
    }];
    
    [dataTask resume];
}

@end
