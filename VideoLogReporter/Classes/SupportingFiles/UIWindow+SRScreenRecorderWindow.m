//
//  UIWindow+SRScreenRecorderWindow.m
//  VideoLogReporterSample
//
//  Created by Varun P M on 19/04/18.
//  Copyright © 2018 Varun P M. All rights reserved.
//

#import "UIWindow+SRScreenRecorderWindow.h"
#import "SRScreenRecorder.h"

// Extension of UIWindow to handle shake gesture to start/stop recording
@implementation UIWindow (SRScreenRecorderWindow)
BOOL isShowingConfirmationAlert;

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if ([SRScreenRecorder sharedScreenRecorder] && !isShowingConfirmationAlert)
    {
        isShowingConfirmationAlert = YES;
        BOOL isRecording = [SRScreenRecorder sharedScreenRecorder].isRecording;
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:(isRecording ? @"Stop screen recording?" : @"Start screen recording?") preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (isRecording) {
                self.layer.borderColor = nil;
                self.layer.borderWidth = 0;
                [[SRScreenRecorder sharedScreenRecorder] stopRecording];
                isShowingConfirmationAlert = NO;
            }
            else {
                self.layer.borderColor = [UIColor greenColor].CGColor;
                self.layer.borderWidth = 4;
                [[SRScreenRecorder sharedScreenRecorder] startRecording];
                isShowingConfirmationAlert = NO;
            }
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            isShowingConfirmationAlert = NO;
        }];
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        
        [[[SRScreenRecorder sharedScreenRecorder] topViewController:self.rootViewController] presentViewController:alertController animated:YES completion:nil];
    }
}

@end
