//
//  JIRAIssueViewController.h
//  ObjectiveDropboxOfficial
//
//  Created by Varun P M on 09/07/18.
//

#import <UIKit/UIKit.h>

@interface JIRAIssueViewController : UIViewController

@property (strong, nonatomic) NSString *apiToken;
@property (strong, nonatomic) NSString *projectKey;
@property (strong, nonatomic) NSString *parentIssue;
@property (strong, nonatomic) NSString *assigneeUsername;
@property (strong, nonatomic) NSString *reporterMailID;
@property (strong, nonatomic) NSArray<NSURL *> *filePaths;
@property (assign, nonatomic) BOOL hasCrashed;

@end
